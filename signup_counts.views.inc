<?php

/*
 * @file
 */

/**
 * Implementation of hook_views_data().
 */
function signup_counts_views_data() {
  $data['signup_counts']['table']['group']  = t('Signup');

  $data['signup_counts']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['signup_counts']['count'] = array(
    'title' => t('Signup total'),
    'help' => t('Total count of signups for a node'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}